//[SECTION] Javascript synchronous vs Asynchronous
//Asynchrounous means that we can proceed to execute other statements, while time consuming code is running in the background
//Fetch() method returns a promise that resolves to a response object 
//promise- is an object that represents the eventual commpletion (or failure) of an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

//fetch() method returns a promise that resolves to a "response object"
fetch('https://jsonplaceholder.typicode.com/posts')
//then() captures the "response object" and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));


fetch('https://jsonplaceholder.typicode.com/posts')
// "json()"- from the "response object" to convert the data retrieved into json format to be used in our application
.then(response => response.json())
//Print the converted json value from the "fetch" request
.then((json)=> console.log(json))
//"promise chain" - using multiple .then()


//async and await

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	// Print out the content of the "response" object
	console.log(json);
}
fetchData();




// [SECTION] Creating a post

fetch('https://jsonplaceholder.typicode.com/posts', {
	//sets the method of the "request object"
	method: 'POST',
	//Sets the header data of the "request obejct" to be sent to the backend
	headers: {'Content-Type': 'application/json'},
	//JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userId": 1,
        "title": "Create Post",
        "body": "Create Post File"
	})
})
.then((response)=> response.json())
.then((json) => console.log(json))

// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/100', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		"userId": 1,
        "title": "Update Post",
        "body": "Update Post File"
	})
})
.then((response)=> response.json())
.then((json) => console.log(json))


//[SECTION] Deleting a post

fetch('https://jsonplaceholder.typicode.com/posts/100',{
	method: 'DELETE'
});























