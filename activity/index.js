async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos');
	
	let json = await result.json();
	let arr1 = json.map((out)=>{
		return out.title
	})
	//console.log(arr1)
}
fetchData();



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response)=> response.json())
.then((json) => {
	console.log(json)
	console.log(`Title: ${json.title}
Status: ${json.completed}`)
})


fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		"title": 'This is a new to do list',
		"userId": 1,
		"completed": false
	})
})
.then((response)=> response.json())
.then((json) => {
	console.log(json)
})

fetch('https://jsonplaceholder.typicode.com/todos/200', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		"title": 'This is an updated to do list',
		"userId": 1,
		"status": false,
		"description": "nothing to put",
		"date completed": "just now"
	})
})
.then((response)=> response.json())
.then((json) => {
	console.log(json)
})

fetch('https://jsonplaceholder.typicode.com/todos/200', {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		"title": 'This is a patched to do list',
		"userId": 1,
		"status": "complete",
		"description": "nothing to put",
		"date completed": "after just now"
	})
})
.then((response)=> response.json())
.then((json) => {
	console.log(json)
})

fetch('https://jsonplaceholder.typicode.com/todos/200',{
	method: 'DELETE'
});









